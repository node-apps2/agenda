const mongoose = require('mongoose')

let Schema = mongoose.Schema;

let ContactosSchema = new Schema({
    nombre: {
        type: String,
        required: [true, 'El nombre es requerido']
    },
    telefono: {
        type: String,
        required: [true, 'El telefono es requerido']
    },
    email: {
        type: String,
        unique: true,
        required: [true, 'El correo electronico es requerido']
    },
    fecha_nac: {
        type: String,
        required: [true, 'La fecha de nacimiento es requerida']
    }
});



module.exports = mongoose.model('contactos', ContactosSchema)