//==========================
//puerto
//==========================
process.env.PORT = process.env.PORT ? process.env.PORT : 3000;
//==========================
//entorno
//==========================
process.env.NODE_ENV = process.env.NODE_ENV || 'dev';

//==========================
//cadena de conexion para mongoose
//==========================
let urlDB = (process.env.NODE_ENV === 'dev') ? 'mongodb://localhost:27017/task' : process.env.MONGO_URI
process.env.URLDB = urlDB

