const express = require('express');
const app = express();
const _ = require('underscore')
const { validationResult } = require('express-validator');
const { validacion } = require('../validators/validador')

const Contacto = require('../model/contacto')


app.post('/crear-contacto',validacion, (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ ok: false, errors: errors.array() });
    }
    let body = _.pick(req.body, ['nombre', 'telefono', 'email', 'fecha_nac']);

    let contacto = new Contacto({
        nombre: body.nombre,
        telefono: body.telefono,
        email: body.email,
        fecha_nac: body.fecha_nac,
    })


    contacto.save((err, contactodb) => {

        if (err) {
            return res.status(500).json({ ok: false, message: 'Ha ocurrido un error' });
        }
        if (!contactodb) {
            return res.status(400).json({
                ok: false,
                message: 'ha ocurrido un error al registrarte'
            })
        }

        res.json({ ok: true, contacto: contactodb })

    })

});



app.get('/contactos', (req, res) => {
    Contacto.find({})
        .exec((err, contactosdb) => {
            if (err) {
                return res.status(500).json({ ok: false, err })
            }
            if (!contactosdb) {
                return res.status(400).json({ ok: false, err: { message: 'no hay contactos' } })
            }
            res.json({ ok: true, contactos: contactosdb })
        })
})




module.exports = app