const { check } = require('express-validator');
const Contacto = require('../model/contacto')


let validacion = [
    check('nombre').not().isEmpty().trim().escape().withMessage('El nombre es necesario'),
    check('telefono').not().isEmpty().trim().escape().withMessage('El telefono es necesario'),
    check('email').isEmail().withMessage('El correo electronico no es valido').custom(value => {
        return Contacto.findOne({ email: value }).then(user => {
            if (user) {
                return Promise.reject('El correo electronico ya existe');
            }
        });
    }).normalizeEmail(),
    check('fecha_nac').custom(value => {
        if (!value.match(/^\d{4}-\d{2}-\d{2}$/)) {
            throw new Error('La fecha de nacimiento es incorrecta');
        }
        const date = new Date(value);
        if (!date.getTime()) throw new Error('La fecha de nacimiento es incorrecta');
        return date.toISOString().slice(0, 10) === value;
    }),
];

module.exports = { validacion }